var HtmlWebpackPlugin = require('html-webpack-plugin');


const config = {
    entry: './main.js',
    output: {
        path:'/',
        filename: 'index.js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
    ],
    devServer: {
        inline: true,
        port: 8006
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test:/\.css$/,
                use:['style-loader','css-loader']
            },
            { test: /\.(jpg|png|woff|woff2|eot|ttf|svg)$/,
                use: { loader: 'url-loader?limit=100000', }, }

        ]
    },
    mode: 'development'
}
module.exports = config;