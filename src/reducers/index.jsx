import {GET_DATA} from "../constants/action-types.jsx";



const initialState = {
    schoolName:'',
    standardName:'',
    studentName : [
        {name:'Tom',school:'Oliver Hansen',roll_no:'1',standard:'I',marks:20} ,
        {name:'Edward',school:'Oliver Hansen',roll_no:'2',standard:'II',marks:30} ,
        {name:'Mr. Bean',school:'Van Henry',roll_no:'3',standard:'I',marks:25} ,
        {name:'Teady',school: 'Carlos Abbott',roll_no:'4',standard:'II',marks:25},
        {name:'Snow Ball',school:'Oliver Hansen',roll_no:'5',standard:'III',marks:26} ,
        {name:'Alvin',school:'Van Henry',roll_no:'6',standard:'IV',marks:22},
        {name:'Noddy',school: 'Ralph Hubbard',roll_no:'7',standard:'I',marks:24},
        {name:'Oscar',school: 'Omar Alexander',roll_no:'8',standard:'V',marks:21} ,
        {name:'Silky',school:'Oliver Hansen',roll_no:'9',standard:'VI',marks:24} ,
        {name:'Scoober',school: 'Ralph Hubbard',roll_no:'10',standard:'VII',marks:25} ,
        {name:'fifi',school: 'Omar Alexander',roll_no:'11',standard:'VIII',marks:27},
        {name:'dobby',school:'Oliver Hansen',roll_no:'12',standard:'I',marks:21} ,
        {name:'Richard',school:'Carlos Abbott',roll_no:'13',standard:'I',marks:28},
        {name:'Adora',school:'Oliver Hansen',roll_no:'14',standard:'II',marks:29},
    ],
    total:0
};
const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DATA:{
            let total=0;
            if(action.payload.standardName && action.payload.schoolName ){
                console.log("both");
                total=0;
                let data=initialState.studentName;
                let filterData = [];
                for(let i=0,k=0;i<data.length;i++){
                    if(data[i].standard === action.payload.standardName && data[i].school === action.payload.schoolName){
                        total +=data[i].marks;
                        filterData[k] = data[i];
                        k++;
                    }
                  
                }
                let result={schoolName:action.payload.schoolName,
                    standardName:action.payload.standardName,
                    studentName:filterData,
                    total} 
    
                 return result;
            }else if(action.payload.schoolName){
                console.log("only school");
                total=0;
                let data=initialState.studentName;
                let filterData = [];
                for(let i=0,k=0;i<data.length;i++){
                    if(data[i].school === action.payload.schoolName){
                       total +=data[i].marks;
                       filterData[k] = data[i];
                       k++;}
                }
                let result={schoolName:action.payload.schoolName,
                    standardName:action.payload.standardName,
                    studentName:filterData,
                    total} 
    
                 return result;
            }
            else if(action.payload.standardName){
                console.log("only standard");
                total =0;
                let data=initialState.studentName;
                let filterData = [];
                for(let i=0,k=0;i<data.length;i++){
                    if(data[i].standard === action.payload.standardName){
                        total +=data[i].marks;
                        filterData[k] = data[i];
                        k++;
                    }
                    
                }
                let result={schoolName:action.payload.schoolName,
                    standardName:action.payload.standardName,
                    studentName:filterData,
                    total} 
    
                 return result;
            } 
        }

        default:
            return state;
    }
};
export default rootReducer;