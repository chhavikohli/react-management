import {GET_DATA} from "../constants/action-types.jsx";
export const getData = data => ({ type: GET_DATA, payload: data });

