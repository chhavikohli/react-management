import React,{ Component } from "react";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
const mapStateToProps = state => {
    console.log("state--", state);
    return {state};
};

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});



class SimpleTable extends Component {
    constructor() {
        super();
    }
    render() {
        const { classes, theme } = this.props;

    return (
            <Paper className={classes.root}>
             <h1>{this.props.state.schoolName}</h1>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Student Name</TableCell>
                            <TableCell numeric>Roll No.</TableCell>
                            <TableCell numeric>Marks</TableCell>


                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.state.studentName.map(row => {
                            return (
                                <TableRow key={row.roll_no}>
                                    <TableCell component="th" scope="row">
                                        {row.name}
                                    </TableCell>
                                    <TableCell numeric >{row.roll_no}</TableCell>
                                    <TableCell numeric>{row.marks}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    <TableBody>
                       
                                <TableRow key={1}>
                                    <TableCell numeric>Total = {this.props.state.total}</TableCell>
                                </TableRow>
                           
                    </TableBody>
                </Table>
            </Paper>
        );
    }

}

SimpleTable.propTypes = {
    classes: PropTypes.object.isRequired,
};
const getTable = connect(mapStateToProps)(SimpleTable);
export default withStyles(styles)(getTable);