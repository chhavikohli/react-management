import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { getData } from "../actions/index.jsx";
let schoolName='',standardName='';


const mapDispatchToProps = dispatch => {
    return {
        getData : data => dispatch(getData(data))
    };
};

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: 300,
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const shoolNames = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];
const standardNames = [
    'I',
    'II',
    'III',
    'IV',
    'V',
    'VI',
    'VII',
    'VIII',
    'IX',
    'X',
];


class SimpleSelect extends Component {
    constructor() {
        super();
        this.state = {
            schoolName:"",
            standardName:"",
            studentName:'',
            total:0
        };
        this.handleSchoolChange = this.handleSchoolChange.bind(this);
        this.handleStandardChange = this.handleStandardChange.bind(this);
    }

    handleSchoolChange(event) {
        this.setState({ schoolName: event.target.value });
        this.state.schoolName= event.target.value;
        event.preventDefault();
        console.log(this.state);
        schoolName  = this.state;
        this.props.getData(schoolName);
    }
    handleStandardChange(event) {
        this.setState({ standardName: event.target.value });
        this.state.standardName= event.target.value;
        event.preventDefault();
         standardName = this.state;
        this.props.getData(standardName);
    }


    render() {
        const { classes, theme } = this.props;

        return (
            <div className={classes.root}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="select-school">SchoolName</InputLabel>
                    <Select
                        value={this.state.schoolName}
                        onChange={this.handleSchoolChange}
                        input={<Input id="select-school" />}
                        MenuProps={MenuProps}
                    >
                        {shoolNames.map(schoolName => (
                            <MenuItem
                                key={schoolName}
                                value={schoolName}
                                style={{
                                    fontWeight:
                                        this.state.schoolName.indexOf(schoolName) === -1
                                            ? theme.typography.fontWeightRegular
                                            : theme.typography.fontWeightMedium,
                                }}
                            >
                                {schoolName}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="select-school">ClassName</InputLabel>
                    <Select
                        value={this.state.standardName}
                        onChange={this.handleStandardChange}
                        input={<Input id="select-school" />}
                        MenuProps={MenuProps}
                    >
                        {standardNames.map(standardName => (
                            <MenuItem
                                key={standardName}
                                value={standardName}
                                style={{
                                    fontWeight:
                                        this.state.standardName.indexOf(standardName) === -1
                                            ? theme.typography.fontWeightRegular
                                            : theme.typography.fontWeightMedium,
                                }}
                            >
                                {standardName}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
        );
    }
}

SimpleSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
const Form = connect(null, mapDispatchToProps)(SimpleSelect);
export default withStyles(styles, { withTheme: true })(Form);