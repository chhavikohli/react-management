import React, { Component,Fragment } from "react";
import Header from './components/Header.jsx';
import Footer from './components/Footer.jsx';
import Home from './components/Home.jsx';


class App extends Component{
    render(){
        return (
                <Fragment>
                    <div><Header/></div>
                    <div className="App-Body">
                        <Home/>
                    </div>

                    <div><Footer/></div>
                </Fragment>

        )
    }

}

export default App;